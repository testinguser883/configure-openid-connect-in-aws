# Configure OpenID Connect (OIDC) between AWS and GitLab


## Use-cases
* Retrieve temporary credentials from AWS to access cloud services
* Use credentials to retrieve secrets or deploy to an environment
* Scope role to branch or project

For additional details, see [documentation here](https://docs.gitlab.com/ee/ci/cloud_services/aws/)


## Steps
1. Fork or clone this project into your GitLab environment. Continue to follow the [AWS documentation](https://docs.gitlab.com/ee/ci/cloud_services/aws/) to configure the connection between AWS and GitLab.

1. Run terraform locally to provision AWS resources. For this demonstration, we are going to create an identiy provider in IAM, a policy to access all S3 resources, and assume policy that will determine which GitLab project can access these resources.

1. Add a `terraform.tfvars` file with your gitlab_url, [aud value, and match value](https://docs.gitlab.com/ee/ci/cloud_services/#configure-a-conditional-role-with-oidc-claims).
    This example would allow any project in the `mygroup` group running a job for the `main` branch.
    ```
    aws_region      = "eu-west-2"
    gitlab_url      = "https://gitlab.example.com"
    aud_value       = "https://gitlab.example.com"
    match_field     = "sub"
    match_value     = ["project_path:mygroup/*:ref_type:branch:ref:main"]
    ```
1.  Run terrorm init, plan, and apply. Now that the AWS resources are provisioned, we will now setup the GitLab project.
1.  Set the ROLE_ARN variable in the GitLab Project (Settings->CI). This value is generated as output after `terraform apply` is run.
1.  Set the AWS_DEFAULT_REGION variable in the GitLab Project (Settings->CI).
1.  Set AWS_PROFILE variable in the GitLab Project (Settings -> CI) to "oidc". [AWS will call STS on our behalf](https://docs.aws.amazon.com/cli/latest/topic/config-vars.html#assume-role-with-web-identity) within the `before_script`.
1.  Our assume role function will fetch temporary STS credentials in the before_script. Within the script, we will test which resources are retrievable. As we only provisioned access to S3, we should receive a success on S3 list calls and failure on EC2 calls. 
1. `MY_OIDC_TOKEN` is an id_token that can be configured. The aud value should be a url descriptive to your service or application. 
1. The job will be marked a failure, but if you look at the log, `aws s3 ls` should execute succesfully but `aws ec2 describe-instances` should fail since we only applied s3 policy permissions to the role.

```yaml
assume-role-example:
  image: 
    name: amazon/aws-cli:latest
    entrypoint: [""]
  id_tokens:
    MY_OIDC_TOKEN:
      aud: https://service-url.example.com
  before_script:
    - mkdir -p ~/.aws
    - echo "${MY_OIDC_TOKEN}" > /tmp/web_identity_token
    - echo -e "[profile oidc]\nrole_arn=${ROLE_ARN}\nweb_identity_token_file=/tmp/web_identity_token" > ~/.aws/config
  script:
    - aws sts get-caller-identity
    - aws s3 ls
    - aws ec2 describe-instances
```

## Resources

- https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_providers_oidc.html
- https://docs.aws.amazon.com/STS/latest/APIReference/API_AssumeRoleWithWebIdentity.html
- https://docs.aws.amazon.com/cli/latest/topic/config-vars.html#assume-role-with-web-identity 
